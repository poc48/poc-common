package com.poc.common.api;

import lombok.Getter;

@Getter
public class ServiceException extends RuntimeException {
    private final String status;
    private final String statusDescription;
    private final String label;
    private final String message;

    public ServiceException(ServiceError error) {
        super(error.getMessage());
        this.status = error.getStatus();
        this.label = error.getLabel();
        this.statusDescription = error.getStatusDescription();
        this.message = error.getMessage();
    }

    public ServiceException(String status, String statusDescription, String label, String message) {
        super(message);
        this.status = status;
        this.label = label;
        this.statusDescription = statusDescription;
        this.message = message;
    }
}
