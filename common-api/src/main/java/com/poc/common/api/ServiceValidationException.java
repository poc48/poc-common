package com.poc.common.api;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ServiceValidationException extends RuntimeException {
    private final String status;
    private final String statusDescription;
    private final List<ServiceError> validationErrors = new ArrayList<>();

    public ServiceValidationException() {
        super("Validation exception.");
        this.status = null;
        this.statusDescription = null;
    }

    public ServiceValidationException(String status, String statusDescription) {
        super("Validation exception.");
        this.status = status;
        this.statusDescription = statusDescription;
    }

    public void addValidationError(ServiceError validationError) {
        this.validationErrors.add(validationError);
    }
}
