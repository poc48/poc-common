package com.poc.common.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class ServiceError implements Serializable {
    private static final long serialVersionUID = 1L;
    private String status;
    private String statusDescription;
    private String label;
    private String message;

    public ServiceError() {
        // For a client's decoder.
    }

    public ServiceError(String label, String message) {
        this.label = label;
        this.message = message;
    }
}
