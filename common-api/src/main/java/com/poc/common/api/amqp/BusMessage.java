package com.poc.common.api.amqp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusMessage implements Serializable {
    @JsonProperty("messageId")
    private String messageId;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("payload")
    private String payload;
}
