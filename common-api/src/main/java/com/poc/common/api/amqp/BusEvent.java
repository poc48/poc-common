package com.poc.common.api.amqp;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * Group exchange, routing key and message type.
 */
public enum BusEvent {
    CATEGORY_CREATED(BusExchanges.CORE, BusRoutingKeys.CATEGORY_CREATED, BusMessage.class),
    CATEGORY_DELETED(BusExchanges.CORE, BusRoutingKeys.CATEGORY_DELETED, BusMessage.class);

    BusEvent(String name, String routingKey, Class<? extends Serializable> type) {
        this.name = name;
        this.routingKey = routingKey;
        this.type = type;
    }

    private final String name;

    private final String routingKey;

    private final Class<? extends Serializable> type;

    public String getName() {
        return name;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public Class<? extends Serializable> getType() {
        return type;
    }

    @Override
    @JsonValue
    public String toString() {
        return name();
    }
}
