package com.poc.common.api.amqp;

/**
 * Set of all exchanges in system.
 */
public final class BusExchanges {
    public static final String CORE = "core";

    private BusExchanges(){
    }
}
