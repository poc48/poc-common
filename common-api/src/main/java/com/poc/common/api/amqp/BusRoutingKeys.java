package com.poc.common.api.amqp;

/**
 * Set of all routing keys in system.
 */
public final class BusRoutingKeys {
    public static final String CATEGORY_CREATED = "core.category.created";
    public static final String CATEGORY_DELETED = "core.category.deleted";

    private BusRoutingKeys() {
    }
}
