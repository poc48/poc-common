package com.poc.common.service.logging;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Component
public class RequestLoggingFilter extends CommonsRequestLoggingFilter {
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        String requestId = request.getHeader("requestId");
        if (requestId == null || "".equals(requestId.trim())) {
            requestId = UUID.randomUUID().toString();
        }
        MDC.put("requestId", requestId);
        super.doFilterInternal(request, response, filterChain);
    }

    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        logger.info(message);
    }

    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        logger.info("Request completed");
    }

    @Override
    protected boolean shouldLog(HttpServletRequest request) {
        return true;
    }

    public RequestLoggingFilter() {
        this.setIncludeQueryString(true);
        this.setIncludeClientInfo(true);
        this.setAfterMessagePrefix("");
    }
}
