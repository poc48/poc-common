package com.poc.common.service.error;

import com.poc.common.api.ServiceError;
import com.poc.common.api.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class contains common service error handling.
 * Controller Advice  in each service should extend this class.
 */
public abstract class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ServiceError handleServiceException(ServiceException ex) {
        LOGGER.error("Service exception", ex);
        ServiceError serviceError = new ServiceError();
        serviceError.setStatus(ex.getStatus());
        serviceError.setLabel("pocerror.service_exception");
        serviceError.setStatusDescription(ex.getStatusDescription());
        serviceError.setMessage(ex.getMessage());
        return serviceError;
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ServiceError handleMissingParamException(MissingServletRequestParameterException ex) {
        LOGGER.error("Missing request property", ex);
        ServiceError serviceError = new ServiceError();
        serviceError.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        serviceError.setLabel("pocerror.missing_property");
        serviceError.setStatusDescription("MissingProperty");
        serviceError.setMessage(ex.getMessage());
        return serviceError;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ServiceError unexpectedException(Exception ex) {
        LOGGER.error("Unexpected error ", ex);
        ServiceError serviceError = new ServiceError();
        serviceError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        serviceError.setLabel("pocerror.unexpected_error");
        serviceError.setStatusDescription(ex.getMessage());
        serviceError.setMessage(ex.getMessage());
        return serviceError;
    }
}
