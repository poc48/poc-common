package com.poc.common.client;

import com.poc.common.api.ServiceError;
import com.poc.common.api.ServiceException;
import feign.Client;
import feign.Feign;
import feign.Request;
import feign.RequestTemplate;
import feign.Response;
import feign.Retryer;
import feign.Target;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;

//CHECKSTYLE:OFF: SingleLineJavadoc
/**
 * @param <T> - Client Interface
 *    <p>
 *    This class is responsible to be parent for all OpenFeign client builders for each of microservices in system.
 *    <strong>Example:</strong> Creating client for document service
 *    </p>
 *    <pre><code class='java'>
 *     public class DocumentFeignClientBuilder extends FeignClientBuilder<DocumentFeignClient> {
 *     public DocumentFeignClientBuilder(String url) {
 *     super(DocumentFeignClient.class, url);}
 *     }
 *
 *     public interface DocumentFeignClient extends DocumentApi {
 *        @Override
 *        @RequestLine("GET /document?id={id}")
 *        DocumentDto getDocById(@Param("id") Integer id);
 *        }
 *    </code></pre>
 */
//CHECKSTYLE:ON: SingleLineJavadoc
public class FeignClientBuilder<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeignClientBuilder.class);

    private static final String EMPTY_RESPONSE_STATUS = "-1";
    private static final String REQUEST_ID = "requestId";


    private final Class<T> clientClass;
    private final String url;
    private Client client;
    private feign.Logger.Level feignLogLevel;
    private Retryer retryer;


    public FeignClientBuilder(Class<T> clientClass, String url) {
        this.clientClass = clientClass;
        this.url = url;
    }

    public FeignClientBuilder(Class<T> clientClass, String url, feign.Logger.Level level) {
        this.clientClass = clientClass;
        this.url = url;
        this.feignLogLevel = level;
    }

    public FeignClientBuilder(Class<T> clientClass, String url, feign.Logger.Level level, Retryer retryer) {
        this.clientClass = clientClass;
        this.url = url;
        this.feignLogLevel = level;
        this.retryer = retryer;
    }

    public T build() {
        if (this.client == null) {
            this.client = buildHttpClient();
        }
        if (this.feignLogLevel == null) {
            this.feignLogLevel = feign.Logger.Level.BASIC;
        }
        if (this.retryer == null) {
            this.retryer = Retryer.NEVER_RETRY;
        }
        return buildFeign();
    }

    private Client buildHttpClient() {
        return new ApacheHttpClient();
    }

    private T buildFeign() {
        JacksonDecoder decoder = new JacksonDecoder();
        return Feign.builder()
                .client(this.client)
                .encoder(new JacksonEncoder())
                .decoder(decoder)
                .errorDecoder(((methodKey, response) -> {
                    try {
                        return decode(response, decoder);
                    } catch (Exception e) {
                        return unexpectedException(response, e);
                    }
                }))
                .logger(new Slf4jLogger(clientClass))
                .logLevel(feignLogLevel)
                .retryer(retryer)
                .target(new Target<>() {
                    @Override
                    public Class<T> type() {
                        return clientClass;
                    }

                    @Override
                    public String name() {
                        return clientClass.getSimpleName();
                    }

                    @Override
                    public String url() {
                        return url;
                    }

                    @Override
                    public Request apply(RequestTemplate input) {
                        input.target(url);
                        input.header("Content-Type", "application/json");
                        input.header(REQUEST_ID, MDC.get(REQUEST_ID));
                        return input.request();
                    }
                });
    }

    private Exception decode(Response response, JacksonDecoder decoder) throws IOException {
        ServiceError error = (ServiceError) decoder.decode(response, ServiceError.class);
        if (error == null) {
            return unexpectedException(response, null);
        }
        return new ServiceException(error);
    }

    private static Exception unexpectedException(Response response, Exception e) {
        if (e != null) {
            LOGGER.error("Client error: ", e);
        }
        return new ServiceException(response == null ? EMPTY_RESPONSE_STATUS : String.valueOf(response.status()),
                null,
                "poc.client.error",
                "Client have not recognized response from service or another client error"
        );
    }
}
