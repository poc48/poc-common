package com.poc.common.amqp;

import com.poc.common.api.amqp.BusEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

import static java.lang.String.format;

@Getter
@Setter
@ToString
public class AmqpMessage<T extends Serializable> {
    private BusEvent busEvent;
    private T payload;

    private AmqpMessage(BusEvent event, T payload) {
        this.busEvent = event;
        this.payload = payload;
    }

    /**
     * Factory method to instantiate {@link AmqpMessage} object.
     * Checks whether the type of payload matches to the event type.
     *
     * @param event   Event type.
     * @param payload Event payload.
     * @param <E>     The type of payload.
     * @return The message object.
     */
    public static <E extends Serializable> AmqpMessage<E> create(BusEvent event, E payload) {
        if (event.getType() != payload.getClass()) {
            throw new IllegalArgumentException(
                    format(
                            "The type of payload '%s' doesn't match to the event '%s'",
                            payload.getClass(),
                            event.getType())
            );
        }
        return new AmqpMessage<>(event, payload);
    }
}
