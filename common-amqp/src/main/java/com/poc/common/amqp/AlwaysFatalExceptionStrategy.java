package com.poc.common.amqp;

import org.springframework.amqp.rabbit.listener.FatalExceptionStrategy;

/**
 * All the exceptions won't go to any queue.
 */
public class AlwaysFatalExceptionStrategy implements FatalExceptionStrategy {
    @Override
    public boolean isFatal(Throwable t) {
        return true;
    }
}
