package com.poc.common.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.common.api.ServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RabbitMessagingService implements MessagingService {
    private RabbitMessagingTemplate messagingTemplate;
    private ObjectMapper objectMapper;

    /**
     * Converts message to json format and sends to the rabbitMq exchange point.
     * The name of exchange point will be obtained from the <code>eventType</code> parameter.
     *
     * @param message the message to pass to the rabbitmq exchange.
     * @param <T>     the type of message payload.
     */
    public <T extends Serializable> void send(AmqpMessage<T> message) {
        String json = asJson(message.getPayload());
        this.messagingTemplate.convertAndSend(
                message.getBusEvent().getName(),
                message.getBusEvent().getRoutingKey(),
                json
        );
    }

    private String asJson(Serializable obj) {
        String result;
        try {
            result = objectMapper.writeValueAsString(obj);
        } catch (IOException e) {
            throw new ServiceException("Unexpected error.", "", "poc.amqp.error", "Message parsing exception.");
        }
        return result;
    }
}
