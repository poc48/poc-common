package com.poc.common.amqp;

import java.io.Serializable;

/**
 * Declare a method to send messages to the AMQP exchange point.
 */
public interface MessagingService {
    /**
     * Sends a message to the exchange point.
     * The destination exchange will be determined based on <code>eventType</code>.
     *
     * @param <T> Type of payload to pass to the exchange point.
     */
    <T extends Serializable> void send(AmqpMessage<T> message);
}
