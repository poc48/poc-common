package com.poc.common.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.common.api.amqp.BusEvent;
import com.poc.common.api.amqp.BusMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RabbitMessagingServiceTest {

    @Mock
    private RabbitMessagingTemplate messagingTemplate;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private RabbitMessagingService rabbitMessagingService;

    @Test
    void shouldSend() throws Exception {
        doNothing().when(messagingTemplate).convertAndSend(anyString(), anyString(), anyString());
        when(objectMapper.writeValueAsString(any())).thenReturn("test");

        BusMessage message = new BusMessage();
        message.setMessageId(UUID.randomUUID().toString());
        AmqpMessage<BusMessage> amqpMessage = AmqpMessage.create(BusEvent.CATEGORY_CREATED, message);

        rabbitMessagingService.send(amqpMessage);
        verify(messagingTemplate, times(1))
                .convertAndSend(anyString(), anyString(), anyString());
        verify(objectMapper, times(1)).writeValueAsString(any());
        verifyNoMoreInteractions(messagingTemplate, objectMapper);
    }
}
