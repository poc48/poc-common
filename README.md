# Commons
Common libraries for microservices.
* Service - all common service, mostly infrastructure like error handling, logging, cache, etc.
* Client - HTTP client for cross service communication.
* API - basic request\response DTOs.
* AMQP - Message generic and RabbitMQ configuration.

# Gradle Dependency
* Client - `implementation 'com.poc:common-client:<version>`
* API - `implementation 'com.poc:common-api:<version>`
* Service - `implementation 'com.poc:common-service:<version>`
* AMQP - `implementation 'com.poc:common-amqp:<version>`

# SonarQube
* Go to https://sonarcloud.io/organizations/poc48/projects

# Checkstyle
* Install [IDE plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea).
* Add a custom styles by selecting the `config/checkstyle/checkstyle.xml`.

# Deployment
* Increment version.
* Run pipeline on `master` branch.
